/* eslint-disable radix */
const express = require("express");
const axios = require("axios");
global.fetch = require("node-fetch");
const moment = require("moment");
require("moment-timezone");
const router = express.Router();
const uuidv4 = require("uuid/v4");
const ElasticSearch = require("../common/es");
const es = new ElasticSearch(
  "https://search-pmftc-philipmorris-stg-b3j6h26w7rdx6hiuw2sytjvyaq.ap-southeast-1.es.amazonaws.com"
);

router.get("/env", async (req, res) => {
  let envVariables = {
    client_secret: process.env.clientSecret,
    client_id: process.env.clientId,
    username: process.env.user_name,
    password: process.env.password,
    authApi: process.env.authApi,
    addRewardApi: process.env.addRewardsApi,
  };
  res.send(envVariables);
});

router.post("/obtain-auth-token", async (req, res) => {
  let payload = {};
  let response;
  let status = 500;
  let message = "Internal Server Error";
  try {
    await axios({
      method: "POST",
      url: process.env.oAuthTokenApi,
      config: {
        headers: {
          "Content-Type": "application/json",
        },
      },
      data: req.body,
    })
      .then((res) => {
        console.log("RES!", res.data);
        response = res.data;
      })
      .catch((err) => {
        console.log("ERROR!", err);
      });

    if (response) {
      payload = response;
      status = 200;
      message = "Success";
    }
  } catch (error) {
    console.log();
    payload, (message = "Internal Server Error");
  }
  res.status(status).send({
    payload,
    message,
    status,
  });
});

router.post("/week", async (req, res) => {
  let payload = {};
  let response;
  let status = 500;
  let message = "Internal Server Error";
  try {
    let currentDate = moment();
    let toFind;
    let urlToFind;
    console.log("CUR_DATE_INFO", currentDate);
    console.log("FIRST_DATE_INFO", moment(req.body.first_date));
    console.log(
      "NO_OF_WEEKS_INFO",
      parseInt(currentDate.diff(moment(req.body.first_date), "week")) + 1
    );
    switch (moment().format("dddd")) {
      case "Monday":
        toFind = "image/iconstofind-herringbone.png";
        urlToFind = "herringbone";
        break;
      case "Tuesday":
        toFind = "image/iconstofind-crest.png";
        urlToFind = "crest";
        break;
      case "Wednesday":
        toFind = "image/iconstofind-PMlogo.png";
        urlToFind = "logo";
        break;
      case "Thursday":
        toFind = "image/iconstofind-PMSignature.png";
        urlToFind = "signature";
        break;
      case "Friday":
        toFind = "image/iconstofind-RedPack.png";
        urlToFind = "100sPack";
        break;
      case "Saturday":
        toFind = "image/iconstofind-Gentleman.png";
        urlToFind = "PMKsPack";
        break;
      case "Sunday":
        toFind = "image/iconstofind-FirmStick.png";
        urlToFind = "firmstick";
        break;
    }
    response = {
      week: parseInt(currentDate.diff(moment(req.body.first_date), "week")) + 1,
      toFind,
      urlToFind,
    };
    if (response) {
      payload = response;
      status = 200;
      message = "Success";
    }
  } catch (error) {
    console.log("ERROR_INFO", error);
    payload, (message = "Internal Server Error");
  }
  res.status(status).send({
    payload,
    message,
    status,
  });
});

router.post("/add-reward-points", async (req, res) => {
  let payload = {};
  let response;
  let status = 500;
  let message = "Internal Server Error";
  try {
    console.log("Req body", req.body);
    const game_record = Object.assign({
      id: uuidv4(),
      created_at: moment().tz("Asia/Manila").format("YYYY-MM-DD HH:mm:ss"),
      spice_id: req.body.addRewardsParams.spice_person_id,
      reward_points: req.body.addRewardsParams.amount,
    });
    response = await es.create(
      process.env.gameRecord_ES,
      game_record,
      game_record.id
    );
    if (response === "created") {
      let getAuthToken = await axios.post(
        "/api/obtain-auth-token",
        req.body.getTokenParams
      );
      if (getAuthToken.data.payload.access_token) {
        req.body.addRewardsParams.game_reference_id = game_record.id;
        console.log("ROBERTO", getAuthToken.data.payload.access_token);
        console.log("req.body.params", req.body.addRewardsParams);
        await axios({
          method: "POST",
          url: process.env.gameTransactionApi,
          data: JSON.stringify(req.body.addRewardsParams),
          headers: {
            Authorization: `Bearer ${getAuthToken.data.payload.access_token}`,
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        })
          .then((res) => {
            console.log("RES!", res.data);
            response = res.data;
          })
          .catch((err) => {
            console.log("ERROR!", err.response.config.config);
            console.log("ERROR!", err.response);
          });
      }
    }

    if (response) {
      payload = response;
      status = 200;
      message = "Success";
    }
  } catch (error) {
    console.log(error);
    payload, (message = "Internal Server Error");
  }
  res.status(status).send({
    payload,
    message,
    status,
  });
});

module.exports = router;
