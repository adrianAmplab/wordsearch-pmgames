let initialOrientation = window.orientation;

$(document).ready(function () {
  $("#btn-playWS").on("click", function () {
    console.log("btn - playWS");
    window.location.href = "/home";
  });
  $("body").on("contextmenu", function (e) {
    return false;
  });
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    // true for mobile device
    console.log("Is Mobile Device?", true);
    $("body").height(window.innerHeight);
    $("body").width(window.innerWidth);
    doOnOrientationChange();
  } else {
    // false for not mobile device
    console.log("Is Mobile Device?", false);
  }
});
function doOnOrientationChange() {
  switch (window.orientation) {
    case -90:
      $("#landscape").css("display", "block");
      $("body").css("overflow", "hidden");
      $("main").css("display", "none");
      break;
    case 90:
      $("#landscape").css("display", "block");
      $("body").css("overflow", "hidden");
      $("main").css("display", "none");
      break;
    default:
      if (initialOrientation == 0) {
        $("#landscape").css("display", "none");
        $("body").css("overflow", "visible");
        $("main").css("display", "");
      } else {
        window.location.reload();
      }

      break;
  }
}
window.addEventListener("orientationchange", doOnOrientationChange);
