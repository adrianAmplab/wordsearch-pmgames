$(document).ready(function () {
  showGameContent();
  $("#go-to-step2-m").on("click", function () {
    history.pushState({}, null, "/step&step=2");
    showGameContent();
  });
  $("#go-to-step2-mb").on("click", function () {
    history.pushState({}, null, "/step&step=2");
    showGameContent();
  });
  $("#go-to-step2").on("click", function () {
    history.pushState({}, null, "/step&step=2");
    showGameContent();
  });
  $("#go-to-step3-m").on("click", function () {
    history.pushState({}, null, "/step&step=3");
    showGameContent();
  });
  $("#go-to-step5-m").on("click", function () {
    $(".game-content--body").css("display", "none");
    $("#step-5-end").css("display", "flex");
  });
  $(".return-to-game").on("click", function () {
    $("#step-5").css("display", "flex");
    $("#step-5-end").css("display", "none");
  });
  $("#try-again-tu-m").on("click", function () {
    history.pushState({}, null, "/step&step=3");
    showGameContent();
  });
  $("#go-to-step3").on("click", function () {
    won_sfx.muted = true;
    won_sfx.play();
    iconFoundSfx.muted = true;
    iconFoundSfx.play();
    wrongAnswerSfx.muted = true;
    wrongAnswerSfx.play();
    history.pushState({}, null, "/step&step=5");
    showGameContent();
  });
  $("#go-to-game").on("click", function () {
    let urltoFind = $(this).attr("data-saved").split("++")[0];
    let week = $(this).attr("data-saved").split("++")[1];
    history.pushState({}, null, `/countdown&icon=${urltoFind}&week=${week}`);
    showGameContent();
    showGameImage(urltoFind, week);
  });
  $("#step-2-close").on("click", function () {
    history.pushState({}, null, "/step&step=2&show=modal");
    showGameContent();
  });
  $("#step-3-close").on("click", function () {
    history.pushState({}, null, "/step&step=3&show=modal");
    showGameContent();
  });

  $(".btn-no").on("click", function () {
    $(".modal").hide();
  });
  $("#try-again-step3").on("click", function () {
    history.pushState({}, null, "/step&step=3");
    showGameContent();
  });
  $("#back-to-step2").on("click", function () {
    history.pushState({}, null, "/step&step=2");
    showGameContent();
  });
});

function showGameContent() {
  $(".popup-won").hide();
  $(".game-content--body").css("display", "none");
  let path = window.location.pathname;
  switch (path) {
    case "/":
    case "/home":
      console.log("/home");
      $("#home-game-content").css("display", "");
      $("#unmute-audio-homepage").css("display", "");
      break;
    case "/step&step=2":
      console.log("/step&step=2");
      $("#step2-game-content").css("display", "");
      $("#unmute-audio-homepage").css("display", "none");
      break;
    case "/step&step=2&show=modal":
      console.log("/step&step=2&show=modal");
      $("#step-2-end").css("display", "flex");
      $("#unmute-audio-homepage").css("display", "none");
      break;
    case "/step&step=3":
      console.log("/step&step=3");
      $("#step3-game-content").css("display", "");
      $("#unmute-audio-homepage").css("display", "none");
      break;
    case "/step&step=3&show=modal":
      console.log("/step&step=3&show=modal");
      $("#step-3-end").css("display", "flex");
      $("#unmute-audio-homepage").css("display", "none");
      break;
    default:
      console.log("def");
      $("#step-5").css("display", "flex");
      $("#unmute-audio-homepage").css("display", "none");
  }
}

window.goIndex = function () {
  $(".modal").modal("hide");
  window.location.href = "https://philipmorris.ph/home";
};
window.goHome = function () {
  $(".modal").modal("hide");
  history.pushState({}, null, `/home`);
  showGameContent();
};

window.goSpendCoins = function () {
  $(".modal").modal("hide");
  window.location.href = "https://philipmorris.ph/play-spin-wheel";
};
