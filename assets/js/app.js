/* eslint-disable no-undef */
/* eslint-disable func-names */
/* eslint-disable no-restricted-syntax */
let intervals;
let interval;
let hasWon = false;
let deviceWidth;

let btnClickSound = new Audio("/audio/click-buttons.mp3");
let wrongAnswerSfx = new Audio(
  "/audio/Warning Beep Sound-Soft Error Effect.mp3"
);
let won_sfx = new Audio("/audio/Win sound effect FREE.mp3");
let iconFoundSfx = new Audio("/audio/Correct Answer SOUND EFFECT2.mp3");

$(document).ready(function () {
  showUnmuteButton();
  window.scrollTo(0, 1);
  paceOptions = {
    ajax: true,
    document: true,
    eventLag: false,
  };
  Pace.on("start", function () {
    $(".game-content").css("opacity", "0.1");
  });
  Pace.on("done", function () {
    $("#preloader").css("display", "none");
    $(".game-content").css("opacity", "1");
    $(".game-content").css("display", "");
  });
  $(".btn").on("click", function () {
    console.log(`btn_click - ${this.id}`);
    btnClickSound.play();
  });
  $("#open_modal").trigger("click");
  $(".modal").modal({
    backdrop: "static",
    keyboard: false,
    show: false,
  });
  deviceWidth = window.innerWidth > 0 ? window.innerWidth : screen.width;
  console.log("width", deviceWidth);

  $(".btn-no").on("click", function () {
    console.log("btn - X");
    $("#gohome").modal("hide");
    $(".pause_icon").text("pause");
    $("#modal_pause").modal("hide");
  });

  $(".return-to-game").on("click", function () {
    console.log("btn - pop up X");
    $("#gohome").modal("hide");
    $(".pause_icon").text("pause");
    $("#modal_pause").modal("hide");
  });

  $(".gameContent-close").on("click", function () {
    console.log("Pause or play");
    $("#modal_pause").modal("show");
  });

  let firstUnmute = 1;
  $("#unmute-audio-homepage").on("click", function () {
    console.log(
      "mute btn - before click - muted prop",
      $(".bg-music").prop("muted")
    );
    $(".bg-music").get(0).play();
    if (firstUnmute == 0) {
      $(".bg-music").prop("muted", false);
      $("#play").removeClass("fas fa-volume-mute");
      $("#play").addClass("fas fa-volume-up");
      firstUnmute = 1;
    } else {
      if ($(".bg-music").prop("muted")) {
        $(".bg-music").prop("muted", false);
        $("#play").removeClass("fas fa-volume-mute");
        $("#play").addClass("fas fa-volume-up");
      } else {
        $(".bg-music").prop("muted", true);
        $("#play").removeClass("fas fa-volume-up");
        $("#play").addClass("fas fa-volume-mute");
      }
    }
  });

  $(".btn-wifi").click(function () {
    $(".wifi-message").show();
  });

  $(".wifi-close").click(function () {
    $(".wifi-message").hide();
  });

  //init Wordsearch
  let pathname = window.location.pathname;
  let variations = 1;
  // if (
  //   pathname.trim().split("&")[1] != undefined &&
  //   pathname.trim().split("&")[1].split("=")[0] === "variations"
  // ) {
  //   variations = pathname.trim().split("&")[1].split("=")[1];
  // }
  variations = moment().isoWeekday();
  if (variations == "7") {
    variations = Math.floor(Math.random() * 6) + 1;
  }
  console.log("Variations Param", variations);
  loadWordSearch(variations);
});

$(window)
  .blur(function () {
    console.log("PAGE_BLUR");
    $(".bg-music").get(0).muted = true;
    //$(".bg-music").get(0).pause();
  })
  .focus(function () {
    console.log("PAGE_FOCUS");
    if ($("#play").hasClass("fas fa-volume-mute") == false) {
      $(".bg-music").get(0).muted = false;
    }
    //$(".bg-music").get(0).play();
  });

$(window).bind("beforeunload", function (event) {
  let pathname = window.location.pathname;
  localStorage.setItem("before_path", pathname);
});

class WordSearch {
  constructor(elementId, settings) {
    this.elementId = elementId;
    this.element = document.getElementById("ws-area");
    this.settings = settings;
    this.solved = 0;
    this.default_settings = {
      directions: ["W", "N", "WN", "EN"],
      gridSize: 12,
      words: settings,
      wordsList: [],
      debug: false,
    };
    this.settings = { ...this.default_settings };

    this.matrix = [];
    this.selectFrom = null;
    this.selected = [];
    this.prevCol = null;
    this.prevRow = null;
    this.hint = [];
    this.isFirstWord = true;
    this.foundWords = [];

    this.SFXfound = "";
    this.SFXwrong = "";
    this.SFXWin = "";

    this.run();
  }

  getHint() {
    return this.hint;
  }

  getFoundWords() {
    return this.foundWords;
  }

  run() {
    if (this.parseWords(this.settings.gridSize)) {
      var isWorked = false;
      while (isWorked == false) {
        this.initialize();
        isWorked = this.addWords();
      }
      if (!this.settings.debug) {
        this.fillUpFools();
      }
      this.drawmatrix();
    }
  }

  parseWords(maxSize) {
    var itWorked = true;
    for (var i = 0; i < this.settings.words.length; i++) {
      this.settings.wordsList[i] = this.settings.words[i].trim();
      this.settings.words[i] = removeDiacritics(
        this.settings.wordsList[i].trim().toUpperCase()
      );

      var word = this.settings.words[i];
      if (word.length > maxSize) {
        alert("The length of word `" + word + "` is overflow the gridSize.");
        console.error(
          "The length of word `" + word + "` is overflow the gridSize."
        );
        itWorked = false;
      }
    }
    return itWorked;
  }

  addWords() {
    var keepGoing = true,
      counter = 0,
      isWorked = true;
    this.hint = [];
    while (keepGoing) {
      // Getting random direction
      var dir = this.settings.directions[
          rangeInt(this.settings.directions.length - 1)
        ],
        result = this.addWord(this.settings.words[counter], dir),
        isWorked = true;
      if (result == false) {
        keepGoing = false;
        isWorked = false;
      }

      counter++;
      if (counter >= this.settings.words.length) {
        keepGoing = false;
      }
    }
    return isWorked;
  }

  addWord(word, direction) {
    var itWorked = true,
      directions = {
        W: [0, 1], // Horizontal (From left to right)
        N: [1, 0], // Vertical (From top to bottom)
        WN: [1, 1], // From top left to bottom right
        EN: [1, -1], // From top right to bottom left
      },
      row,
      col; // y, x

    switch (direction) {
      case "W": // Horizontal (From left to right)
        var row = rangeInt(this.settings.gridSize - 1),
          col = rangeInt(this.settings.gridSize - word.length);
        break;

      case "N": // Vertical (From top to bottom)
        var row = rangeInt(this.settings.gridSize - word.length),
          col = rangeInt(this.settings.gridSize - 1);
        break;

      case "WN": // From top left to bottom right
        var row = rangeInt(this.settings.gridSize - word.length),
          col = rangeInt(this.settings.gridSize - word.length);
        break;

      case "EN": // From top right to bottom left
        var row = rangeInt(this.settings.gridSize - word.length),
          col = rangeInt(word.length - 1, this.settings.gridSize - 1);
        break;

      default:
        var error = "UNKNOWN DIRECTION " + direction + "!";
        alert(error);
        console.log(error);
        break;
    }

    // Add words to the matrix
    for (var i = 0; i < word.length; i++) {
      var newRow = row + i * directions[direction][0],
        newCol = col + i * directions[direction][1];
      var cell = [];
      cell.push(newRow);
      cell.push(newCol);

      // The letter on the board
      var origin = this.matrix[newRow][newCol].letter;
      cell.push(word[i]);

      this.hint.push(cell);

      if (origin == "." || origin == word[i]) {
        this.matrix[newRow][newCol].letter = word[i];
      } else {
        itWorked = false;
      }
    }
    return itWorked;
  }

  initialize() {
    this.initmatrix(this.settings.gridSize);
  }
  initmatrix(size) {
    for (var row = 0; row < size; row++) {
      for (var col = 0; col < size; col++) {
        var item = {
          letter: ".", // Default value
          row: row,
          col: col,
        };

        if (!this.matrix[row]) {
          this.matrix[row] = [];
        }

        this.matrix[row][col] = item;
      }
    }
  }
  drawmatrix() {
    for (var row = 0; row < this.settings.gridSize; row++) {
      // New row
      var divEl = document.createElement("div");
      divEl.setAttribute("class", "ws-row");
      this.element.appendChild(divEl);
      for (var col = 0; col < this.settings.gridSize; col++) {
        var cvEl = document.createElement("canvas");
        cvEl.setAttribute("class", "ws-col");
        cvEl.setAttribute("width", 40);
        cvEl.setAttribute("height", 40);

        // Fill text in middle center
        var x = cvEl.width / 2,
          y = cvEl.height / 2;

        var ctx = cvEl.getContext("2d");
        ctx.font = "400 35px Calibri";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillStyle = "#333"; // Text color
        ctx.fillText(this.matrix[row][col].letter, x, y);
        cvEl.setAttribute("col", col);
        cvEl.setAttribute("row", row);

        var _item = this.matrix[row][col];
        // Add event listeners
        cvEl.addEventListener(
          "mousedown",
          this.onMousedown(this.matrix[row][col])
        );
        cvEl.addEventListener(
          "mouseover",
          this.onMouseover(this.matrix[row][col])
        );
        cvEl.addEventListener("mouseup", this.onMouseup());

        cvEl.addEventListener(
          "touchstart",
          this.onTouchstart(this.matrix[row][col]),
          { passive: false }
        );
        cvEl.addEventListener(
          "touchmove",
          (event) => {
            event.preventDefault();
            var xPos = event.touches[0].pageX;
            var yPos = event.touches[0].pageY;
            var targetElement = document.elementFromPoint(xPos, yPos);
            var _col = parseInt(targetElement.getAttribute("col"));
            var _row = parseInt(targetElement.getAttribute("row"));

            if (
              _col === parseInt(this.prevCol) &&
              _row === parseInt(this.prevRow) &&
              Math.abs(_row - parseInt(this.prevRow)) ==
                Math.abs(_col - parseInt(this.prevCol))
            ) {
              return;
            } else {
              this.prevCol = _col;
              this.prevRow = _row;

              if (this.selectFrom) {
                this.selected = this.getItems(
                  this.selectFrom.row,
                  this.selectFrom.col,
                  _row,
                  _col
                );

                this.clearHighlight();

                for (var i = 0; i < this.selected.length; i++) {
                  var current = this.selected[i],
                    row = current.row + 1,
                    col = current.col + 1;
                  var sel = $(
                    `div.ws-row:nth-child(${row}) canvas.ws-col:nth-child(${col})`
                  );
                  sel.addClass("ws-selected");
                }
              }
            }
          },
          { passive: false }
        );
        cvEl.addEventListener("touchend", this.onTouchend(), {
          passive: false,
        });

        divEl.appendChild(cvEl);
      }
    }
  }
  fillUpFools() {
    var rangeLanguage = searchLanguage(this.settings.words[0].split("")[0]);
    for (var row = 0; row < this.settings.gridSize; row++) {
      for (var col = 0; col < this.settings.gridSize; col++) {
        if (this.matrix[row][col].letter == ".") {
          // rangeInt(65, 90) => A ~ Z
          this.matrix[row][col].letter = String.fromCharCode(
            rangeInt(rangeLanguage[0], rangeLanguage[1])
          );
        }
      }
    }
  }
  getItems(rowFrom, colFrom, rowTo, colTo) {
    var items = [];

    if (
      rowFrom === rowTo ||
      colFrom === colTo ||
      Math.abs(rowTo - rowFrom) == Math.abs(colTo - colFrom)
    ) {
      var shiftY = rowFrom === rowTo ? 0 : rowTo > rowFrom ? 1 : -1,
        shiftX = colFrom === colTo ? 0 : colTo > colFrom ? 1 : -1,
        row = rowFrom,
        col = colFrom;

      items.push(this.getItem(row, col));
      do {
        row += shiftY;
        col += shiftX;
        items.push(this.getItem(row, col));
      } while (row !== rowTo || col !== colTo);
    }
    return items;
  }
  getItem(row, col) {
    return this.matrix[row] ? this.matrix[row][col] : undefined;
  }
  clearHighlight() {
    var selectedEls = document.querySelectorAll(".ws-selected");
    for (var i = 0; i < selectedEls.length; i++) {
      selectedEls[i].classList.remove("ws-selected");
    }
  }
  lookup(selected) {
    var words = [""];

    for (var i = 0; i < selected.length; i++) {
      words[0] += selected[i].letter;
    }
    words.push(words[0].split("").reverse().join(""));
    if (
      this.settings.words.indexOf(words[0]) > -1 ||
      this.settings.words.indexOf(words[1]) > -1
    ) {
      for (var i = 0; i < selected.length; i++) {
        var row = selected[i].row + 1,
          col = selected[i].col + 1;
        var sel = $(
          `div.ws-row:nth-child(${row}) canvas.ws-col:nth-child(${col})`
        );
        sel.removeClass("hint");
        sel.addClass("ws-found");
      }
      if (
        !this.foundWords.includes(words[0]) ||
        !this.foundWords.includes(words[1])
      ) {
        this.SFXfound.muted = false;
        this.SFXfound.play();
        //Increment solved words.
        this.solved++;
        this.foundWords.push(words[0]);
        this.foundWords.push(words[1]);

        //Cross word off list.
        var wordList = $(".ws-word")
          .map(function () {
            if (
              words[0] == removeDiacritics(this.innerHTML.toUpperCase()) ||
              words[1] == removeDiacritics(this.innerHTML.toUpperCase())
            ) {
              console.log("found word", this.innerHTML);
              var match = this;
              match.setAttribute("class", "ws-word wordCheck");
            }
          })
          .get();
      }

      //Game over?
      if (this.solved == this.settings.words.length) {
        this.gameOver();
      }
    } else {
      this.SFXwrong.muted = false;
      this.SFXwrong.play();
      console.log("wrong word selected");
    }
  }
  async gameOver() {
    //Create overlay.
    console.log("Game Over");
    $(".popup-won").css("display", "none");
    this.SFXWin.muted = false;
    this.SFXWin.play();
    hasWon = true;
    $(".game-content--body").css("display", "none");
    $("#step-6").css("display", "flex");

    //Call API - Add Rewards
    try {
      let env = await axios.get("/api/env");
      console.log("Env", env);
      let authData;
      let paramGetToken;
      authData = await fetch(env.data.authApi, {
        credentials: "include",
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => (authData = data))
        .then(async function () {
          console.log(authData);
          if (authData.message == "Unauthenticated.") {
            alert("User unauthenticated");
          } else {
            paramGetToken = {
              grant_type: "password",
              client_id: env.data.client_id,
              client_secret: env.data.client_secret,
              username: env.data.username,
              password: env.data.password,
              scope: "",
            };
            let addRewardsParams = {
              game_reference_id: "16",
              amount: "50",
              spice_person_id: authData.data.spice_person_id,
            };
            let params = {
              addRewardsParams: addRewardsParams,
              getTokenParams: paramGetToken,
            };
            console.log(addRewardsParams);
            let addRewards = await axios.post(env.data.addRewardApi, params);
            console.log("Add rewards response :", addRewards);
            if (addRewards.data.message == "Success") {
              hasWon = true;
            }
          }
        });
    } catch (e) {
      console.log(e);
    }
  }
  onMousedown(item) {
    var _this = this;
    return function () {
      _this.selectFrom = item;
    };
  }

  onTouchstart(item) {
    var _this = this;
    return function () {
      _this.selectFrom = item;
      _this.prevCol = parseInt(item.col);
      _this.prevRow = parseInt(item.row);
    };
  }
  onMouseover(item) {
    var _this = this;
    return function () {
      if (_this.selectFrom) {
        _this.selected = _this.getItems(
          parseInt(_this.selectFrom.row),
          parseInt(_this.selectFrom.col),
          parseInt(item.row),
          parseInt(item.col)
        );

        _this.clearHighlight();

        for (var i = 0; i < _this.selected.length; i++) {
          var current = _this.selected[i],
            row = parseInt(current.row) + 1,
            col = parseInt(current.col) + 1;
          var sel = $(
            `div.ws-row:nth-child(${row}) canvas.ws-col:nth-child(${col})`
          );
          sel.addClass("ws-selected");
        }
      }
    };
  }
  onTouchmove(event, item) {
    var _this = this;

    return function () {
      var xPos = event.touches[0].pageX;
      var yPos = event.touches[0].pageY;
      var targetElement = document.elementFromPoint(xPos, yPos);
      if (_this.selectFrom) {
        _this.selected = _this.getItems(
          _this.selectFrom.row,
          _this.selectFrom.col,
          item.row,
          item.col
        );

        _this.clearHighlight();

        for (var i = 0; i < _this.selected.length; i++) {
          var current = _this.selected[i],
            row = current.row + 1,
            col = current.col + 1,
            el = document.querySelector(
              ".ws-area .ws-row:nth-child(" +
                row +
                ") .ws-col:nth-child(" +
                col +
                ")"
            );

          el.className += " ws-selected";
        }
      }
    };
  }

  onMouseup() {
    var _this = this;
    return function () {
      _this.selectFrom = null;
      _this.clearHighlight();
      _this.lookup(_this.selected);
      _this.selected = [];
    };
  }

  onTouchend() {
    var _this = this;
    return function () {
      _this.selectFrom = null;
      _this.clearHighlight();
      _this.lookup(_this.selected);
      _this.selected = [];
    };
  }
}

var defaultDiacriticsRemovalMap = [
  {
    base: "A",
    letters: /(&#65;|&#9398;|&#65313;|&#192;|&#193;|&#194;|&#7846;|&#7844;|&#7850;|&#7848;|&#195;|&#256;|&#258;|&#7856;|&#7854;|&#7860;|&#7858;|&#550;|&#480;|&#196;|&#478;|&#7842;|&#197;|&#506;|&#461;|&#512;|&#514;|&#7840;|&#7852;|&#7862;|&#7680;|&#260;|&#570;|&#11375;|[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F])/g,
  },
  {
    base: "AA",
    letters: /(&#42802;|[\uA732])/g,
  },
  {
    base: "AE",
    letters: /(&#198;|&#508;|&#482;|[\u00C6\u01FC\u01E2])/g,
  },
  {
    base: "AO",
    letters: /(&#42804;|[\uA734])/g,
  },
  {
    base: "AU",
    letters: /(&#42806;|[\uA736])/g,
  },
  {
    base: "AV",
    letters: /(&#42808;|&#42810;|[\uA738\uA73A])/g,
  },
  {
    base: "AY",
    letters: /(&#42812;|[\uA73C])/g,
  },
  {
    base: "B",
    letters: /(&#66;|&#9399;|&#65314;|&#7682;|&#7684;|&#7686;|&#579;|&#386;|&#385;|[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181])/g,
  },
  {
    base: "C",
    letters: /(&#67;|&#9400;|&#65315;|&#262;|&#264;|&#266;|&#268;|&#199;|&#7688;|&#391;|&#571;|&#42814;|[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E])/g,
  },
  {
    base: "D",
    letters: /(&#68;|&#9401;|&#65316;|&#7690;|&#270;|&#7692;|&#7696;|&#7698;|&#7694;|&#272;|&#395;|&#394;|&#393;|&#42873;|&#208;|[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\u00D0])/g,
  },
  {
    base: "DZ",
    letters: /(&#497;|&#452;|[\u01F1\u01C4])/g,
  },
  {
    base: "Dz",
    letters: /(&#498;|&#453;|[\u01F2\u01C5])/g,
  },
  {
    base: "E",
    letters: /(&#69;|&#9402;|&#65317;|&#200;|&#201;|&#202;|&#7872;|&#7870;|&#7876;|&#7874;|&#7868;|&#274;|&#7700;|&#7702;|&#276;|&#278;|&#203;|&#7866;|&#282;|&#516;|&#518;|&#7864;|&#7878;|&#552;|&#7708;|&#280;|&#7704;|&#7706;|&#400;|&#398;|[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E])/g,
  },
  {
    base: "F",
    letters: /(&#70;|&#9403;|&#65318;|&#7710;|&#401;|&#42875;|[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B])/g,
  },
  {
    base: "G",
    letters: /(&#71;|&#9404;|&#65319;|&#500;|&#284;|&#7712;|&#286;|&#288;|&#486;|&#290;|&#484;|&#403;|&#42912;|&#42877;|&#42878;|[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E])/g,
  },
  {
    base: "H",
    letters: /(&#72;|&#9405;|&#65320;|&#292;|&#7714;|&#7718;|&#542;|&#7716;|&#7720;|&#7722;|&#294;|&#11367;|&#11381;|&#42893;|[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D])/g,
  },
  {
    base: "I",
    letters: /(&#73;|&#9406;|&#65321;|&#204;|&#205;|&#206;|&#296;|&#298;|&#300;|&#304;|&#207;|&#7726;|&#7880;|&#463;|&#520;|&#522;|&#7882;|&#302;|&#7724;|&#407;|[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197])/g,
  },
  {
    base: "J",
    letters: /(&#74;|&#9407;|&#65322;|&#308;|&#584;|[\u004A\u24BF\uFF2A\u0134\u0248])/g,
  },
  {
    base: "K",
    letters: /(&#75;|&#9408;|&#65323;|&#7728;|&#488;|&#7730;|&#310;|&#7732;|&#408;|&#11369;|&#42816;|&#42818;|&#42820;|&#42914;|[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2])/g,
  },
  {
    base: "L",
    letters: /(&#76;|&#9409;|&#65324;|&#319;|&#313;|&#317;|&#7734;|&#7736;|&#315;|&#7740;|&#7738;|&#321;|&#573;|&#11362;|&#11360;|&#42824;|&#42822;|&#42880;|[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780])/g,
  },
  {
    base: "LJ",
    letters: /(&#455;|[\u01C7])/g,
  },
  {
    base: "Lj",
    letters: /(&#456;|[\u01C8])/g,
  },
  {
    base: "M",
    letters: /(&#77;|&#9410;|&#65325;|&#7742;|&#7744;|&#7746;|&#11374;|&#412;|[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C])/g,
  },
  {
    base: "N",
    letters: /(&#78;|&#9411;|&#65326;|&#504;|&#323;|&#209;|&#7748;|&#327;|&#7750;|&#325;|&#7754;|&#7752;|&#544;|&#413;|&#42896;|&#42916;|&#330;|[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4\u014A])/g,
  },
  {
    base: "NJ",
    letters: /(&#458;|[\u01CA])/g,
  },
  {
    base: "Nj",
    letters: /(&#459;|[\u01CB])/g,
  },
  {
    base: "O",
    letters: /(&#79;|&#9412;|&#65327;|&#210;|&#211;|&#212;|&#7890;|&#7888;|&#7894;|&#7892;|&#213;|&#7756;|&#556;|&#7758;|&#332;|&#7760;|&#7762;|&#334;|&#558;|&#560;|&#214;|&#554;|&#7886;|&#336;|&#465;|&#524;|&#526;|&#416;|&#7900;|&#7898;|&#7904;|&#7902;|&#7906;|&#7884;|&#7896;|&#490;|&#492;|&#216;|&#510;|&#390;|&#415;|&#42826;|&#42828;|[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C])/g,
  },
  {
    base: "OE",
    letters: /(&#338;|[\u0152])/g,
  },
  {
    base: "OI",
    letters: /(&#418;|[\u01A2])/g,
  },
  {
    base: "OO",
    letters: /(&#42830;|[\uA74E])/g,
  },
  {
    base: "OU",
    letters: /(&#546;|[\u0222])/g,
  },
  {
    base: "P",
    letters: /(&#80;|&#9413;|&#65328;|&#7764;|&#7766;|&#420;|&#11363;|&#42832;|&#42834;|&#42836;|[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754])/g,
  },
  {
    base: "Q",
    letters: /(&#81;|&#9414;|&#65329;|&#42838;|&#42840;|&#586;|[\u0051\u24C6\uFF31\uA756\uA758\u024A])/g,
  },
  {
    base: "R",
    letters: /(&#82;|&#9415;|&#65330;|&#340;|&#7768;|&#344;|&#528;|&#530;|&#7770;|&#7772;|&#342;|&#7774;|&#588;|&#11364;|&#42842;|&#42918;|&#42882;|[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782])/g,
  },
  {
    base: "S",
    letters: /(&#83;|&#9416;|&#65331;|&#7838;|&#346;|&#7780;|&#348;|&#7776;|&#352;|&#7782;|&#7778;|&#7784;|&#536;|&#350;|&#11390;|&#42920;|&#42884;|[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784])/g,
  },
  {
    base: "T",
    letters: /(&#84;|&#9417;|&#65332;|&#7786;|&#356;|&#7788;|&#538;|&#354;|&#7792;|&#7790;|&#358;|&#428;|&#430;|&#574;|&#42886;|[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786])/g,
  },
  {
    base: "TH",
    letters: /(&#222;|[\u00DE])/g,
  },
  {
    base: "TZ",
    letters: /(&#42792;|[\uA728])/g,
  },
  {
    base: "U",
    letters: /(&#85;|&#9418;|&#65333;|&#217;|&#218;|&#219;|&#360;|&#7800;|&#362;|&#7802;|&#364;|&#220;|&#475;|&#471;|&#469;|&#473;|&#7910;|&#366;|&#368;|&#467;|&#532;|&#534;|&#431;|&#7914;|&#7912;|&#7918;|&#7916;|&#7920;|&#7908;|&#7794;|&#370;|&#7798;|&#7796;|&#580;|[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244])/g,
  },
  {
    base: "V",
    letters: /(&#86;|&#9419;|&#65334;|&#7804;|&#7806;|&#434;|&#42846;|&#581;|[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245])/g,
  },
  {
    base: "VY",
    letters: /(&#42848;|[\uA760])/g,
  },
  {
    base: "W",
    letters: /(&#87;|&#9420;|&#65335;|&#7808;|&#7810;|&#372;|&#7814;|&#7812;|&#7816;|&#11378;|[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72])/g,
  },
  {
    base: "X",
    letters: /(&#88;|&#9421;|&#65336;|&#7818;|&#7820;|[\u0058\u24CD\uFF38\u1E8A\u1E8C])/g,
  },
  {
    base: "Y",
    letters: /(&#89;|&#9422;|&#65337;|&#7922;|&#221;|&#374;|&#7928;|&#562;|&#7822;|&#376;|&#7926;|&#7924;|&#435;|&#590;|&#7934;|[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE])/g,
  },
  {
    base: "Z",
    letters: /(&#90;|&#9423;|&#65338;|&#377;|&#7824;|&#379;|&#381;|&#7826;|&#7828;|&#437;|&#548;|&#11391;|&#11371;|&#42850;|[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762])/g,
  },
  {
    base: "", //delete Niqqud in Hebrew
    letters: /[\u0591-\u05C7]/g,
  },
];

function removeDiacritics(str) {
  for (var i = 0; i < defaultDiacriticsRemovalMap.length; i++) {
    str = str.replace(
      defaultDiacriticsRemovalMap[i].letters,
      defaultDiacriticsRemovalMap[i].base
    );
  }
  return str;
}
//------------------------------Search language--------------------------------------------------//
// Determine what letters injected on grid
function searchLanguage(firstLetter) {
  var codefirstLetter = firstLetter.charCodeAt();
  var codeLetter = [65, 90];
  if (codefirstLetter >= 65 && codefirstLetter <= 90) {
    // Latin
    return (codeLetter = [65, 90]);
  }
  if (codefirstLetter >= 1488 && codefirstLetter <= 1514) {
    //Hebrew א -> ת
    return (codeLetter = [1488, 1514]);
  }
  if (codefirstLetter >= 913 && codefirstLetter <= 937) {
    //Greek Α -> Ω
    return (codeLetter = [913, 929]); //930 is blank
  }
  if (codefirstLetter >= 1040 && codefirstLetter <= 1071) {
    //Cyrillic А -> Я
    return (codeLetter = [1040, 1071]); //930 is blank
  }
  if (codefirstLetter >= 1569 && codefirstLetter <= 1610) {
    //Arab
    return (codeLetter = [1569, 1594]); //Between 1595 and 1600, no letter
  }
  if (codefirstLetter >= 19969 && codefirstLetter <= 40891) {
    //Chinese
    return (codeLetter = [19969, 40891]);
  }
  if (codefirstLetter >= 12354 && codefirstLetter <= 12436) {
    //Japan Hiragana
    return (codeLetter = [12388, 12418]); //Only no small letter
  }
  console.log("Letter not detected : " + firstLetter + ":" + codefirstLetter);
  return codeLetter;
}
function rangeInt(min, max) {
  if (max == undefined) {
    max = min;
    min = 0;
  }
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function renderWordSearch(element, settings) {
  return new WordSearch(element, settings);
}
function loadWordSearch(variations) {
  //let toggleStatus = false;
  let isHintUsed = false;
  let continuosHint = false;
  wordsArray = [
    ["crest", "path", "heritage", "firmstick", "thegentleman"],
    ["hot", "red", "sterling", "silver", "true", "gold"],
    ["goaloriented", "goodlooking", "status", "levelup"],
    ["firm", "filter", "tobacco", "extra", "consistent", "taste"],
    ["imaginative", "aspirational", "confident", "encouraging"],
    ["winning", "grounded", "discerning", "progress"],
    ["menthol", "red", "fresh", "flavor", "green"],
  ];
  console.log("variations", wordsArray[variations]);
  let ws = renderWordSearch("ws-area", wordsArray[variations]);

  //init WS sound assets
  wrongAnswerSfx.muted = true;
  wrongAnswerSfx.play();
  iconFoundSfx.muted = true;
  iconFoundSfx.play();
  won_sfx.muted = true;
  won_sfx.play();
  ws.SFXwrong = wrongAnswerSfx;
  ws.SFXfound = iconFoundSfx;
  ws.SFXWin = won_sfx;

  let resHintArray = restructureHintArray(ws.getHint(), wordsArray[variations]);
  var wordsWrap = $("#tableBody");
  var trEl = "";

  //populate list on table UI
  var columnCounter = 1;
  wordsArray[variations].forEach((word, i) => {
    if (columnCounter == 1) {
      trEl = document.createElement("tr");
    }

    var tdEl = document.createElement("td");
    var liEl = document.createElement("li");
    liEl.setAttribute("class", "ws-word circle");
    liEl.innerText = capitalizeFirstLetter(word);
    tdEl.appendChild(liEl);
    trEl.appendChild(tdEl);
    if (columnCounter == 3) {
      wordsWrap.append(trEl);
      columnCounter = 0;
    }
    columnCounter++;
  });
  wordsWrap.append(trEl);

  $("#hint-btn").on("click", function () {
    console.log("Hint used");
    if (!isHintUsed) {
      if (!continuosHint) {
        isHintUsed = true;
        $("#hint-btn").prop("disabled", true);
      }

      let foundWords = ws.getFoundWords();

      wordsArray[variations].every((word, i) => {
        if (!foundWords.includes(word)) {
          for (var j = 0; j < word.length; j++) {
            var letter = resHintArray[i];
            var row = letter[j][0];
            var col = letter[j][1];
            //if (toggleStatus) {
            //$(`[col="${col}"][row="${row}"]`).removeClass("hint");
            //} else {
            $(`[col="${col}"][row="${row}"]`).addClass("hint");
            //}
          }
          return false;
        } else {
          return true;
        }
      });

      //toggleStatus = !toggleStatus;
    }
  });
}

function restructureHintArray(hintArray, wordsArray) {
  let resHintArray = [];
  const length = wordsArray.length;
  let counter = 0;
  for (var i = 0; i < length; i++) {
    let wordLetterArray = [];
    for (var j = 0; j < wordsArray[i].length; j++) {
      wordLetterArray.push(hintArray[counter]);
      counter++;
    }
    resHintArray.push(wordLetterArray);
  }
  console.log("hint-array-coordinates", resHintArray);
  return resHintArray;
}

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }

  return "unknown";
}

function showUnmuteButton() {
  let ua = navigator.userAgent.toLowerCase();
  if (getMobileOperatingSystem() == "iOS") {
    $(".bg-music").prop("muted", true);
    $("#unmute-audio-homepage").css("display", "");
  } else {
    if (navigator.userAgent.indexOf("Firefox") != -1) {
      $(".bg-music").prop("muted", true);
      $("#unmute-audio-homepage").css("display", "");
    } else if (ua.indexOf("safari") != -1) {
      if (ua.indexOf("chrome") > -1) {
        $(".bg-music").prop("muted", true);
        $("#unmute-audio-homepage").css("display", "");
        // $(".bg-music").removeAttr("muted");
        // $("#unmute-audio-homepage").css("display", "none");
        // $("#play").removeClass("fas fa-volume-mute");
        // $("#play").addClass("fas fa-volume-up");
      } else {
        $(".bg-music").prop("muted", true);
        $("#unmute-audio-homepage").css("display", "");
      }
    } else {
      $(".bg-music").prop("muted", true);
      $("#unmute-audio-homepage").css("display", "");
      // $("#play").removeClass("fas fa-volume-mute");
      // $("#play").addClass("fas fa-volume-up");
    }
  }
}

function isIOS() {
  return (
    [
      "iPad Simulator",
      "iPhone Simulator",
      "iPod Simulator",
      "iPad",
      "iPhone",
      "iPod",
    ].includes(navigator.platform) ||
    // iPad on iOS 13 detection
    (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  );
}

function soundSFX(source) {
  this.sound = document.createElement("audio");
  this.sound.src = source;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.setAttribute("muted", true);
  this.sound.style.display = "none";
  document.body.appendChild(this.sound);
  this.play = function () {
    this.sound.play();
  };
  this.stop = function () {
    this.sound.pause();
  };
  this.muteFn = function (val) {
    this.sound.muted = val;
  };
}
