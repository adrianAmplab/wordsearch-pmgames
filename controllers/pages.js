const axios = require("axios");
exports.pages = async (req, res) => {
  console.log(req);
  let toFindData = await axios.post(
    `${req.protocol}://${req.get("host")}/api/week`,
    { first_date: "2020-09-07" }
  );
  res.render("pages", {
    layout: false,
    data: toFindData.data.payload,
    toFindCount: parseInt(toFindData.data.payload.week) + 2,
  });
};
